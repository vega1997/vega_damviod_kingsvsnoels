package cat.escolapia.damviod.pmdm.KingsvsNoels;

/**
 * Created by david.vega on 21/12/2016.
 */
public class King {
    public int x, y;
    public King(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void goLeft() {
        x -=1;
    }

    public void goRight() {
        x +=1;
    }

    public void goDown() {
        y +=1;
    }

    public void goUp() {
        y -=1;
    }


}
