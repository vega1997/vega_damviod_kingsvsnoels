package cat.escolapia.damviod.pmdm.KingsvsNoels;

/**
 * Created by david.vega on 21/12/2016.
 */
import cat.escolapia.damviod.pmdm.framework.Music;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Sound;

public class Assets {
    public static Pixmap background;
    public static Pixmap king;
    public static Pixmap king2;
    public static Pixmap king3;
    public static Pixmap noel;
    public static Pixmap numbers;
    public static Pixmap mainMenu;
    public static Pixmap pause;
    public static Pixmap buttons;
    public static Pixmap gameOver;

}

