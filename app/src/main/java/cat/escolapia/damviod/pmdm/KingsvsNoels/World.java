package cat.escolapia.damviod.pmdm.KingsvsNoels;

/**
 * Created by david.vega on 21/12/2016.
 */
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 15;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.02f;

    public King king;

    public List<Noel> noelList = new ArrayList<Noel>();
    public boolean gameOver = false;;
    public int score = 0;
    public int bg_score = 0;
    public int lifes = 3;
    public int count = 0;


    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public World() {
        king = new King(8,14);
        noelList.add(new Noel(3, 1));
        noelList.add(new Noel(0, 2));
        noelList.add(new Noel(3, 3));
        noelList.add(new Noel(7, 4));
        noelList.add(new Noel(6, 5));
        noelList.add(new Noel(8, 6));
        noelList.add(new Noel(2, 7));
        noelList.add(new Noel(3, 8));
        noelList.add(new Noel(7, 9));
        noelList.add(new Noel(6, 10));
        noelList.add(new Noel(8, 11));
        noelList.add(new Noel(5, 12));
        noelList.add(new Noel(5, 13));
    }


    public void update(float deltaTime) {
        if (gameOver) return;

        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            int len = noelList.size();
            for(int i = 0; i < len; i++) {
                Noel noels = noelList.get(i);
                noels.advance();
                if(noels.x == king.x && noels.y == king.y) {
                    lifes -= 1;
                    if (lifes == 0) {
                        gameOver = true;
                    } else {
                        king.x = 8;
                        king.y = 14;
                    }

                }

            }
            if(king.y == 0) {
                lifes -= 1;
                // count +=1;
                if (lifes == 0) {
                    gameOver = true;
                } else {
                    king.x = 8;
                    king.y = 14;
                }
            }
        }
    }
}

