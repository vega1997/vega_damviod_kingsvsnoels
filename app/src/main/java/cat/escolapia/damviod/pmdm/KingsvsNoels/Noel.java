package cat.escolapia.damviod.pmdm.KingsvsNoels;

/**
 * Created by David on 15/1/17.
 */

public class Noel {
    public int x, y;

    public Noel(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public void advance() {
            this.x += 1;

            if(this.x > 10)
                this.x = 0;
    }
}