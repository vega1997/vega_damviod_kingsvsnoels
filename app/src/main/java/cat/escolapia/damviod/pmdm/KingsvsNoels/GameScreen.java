package cat.escolapia.damviod.pmdm.KingsvsNoels;

/**
 * Created by david.vega on 21/12/2016.
 */
import java.util.List;

import android.graphics.Color;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Screen;

public class GameScreen extends Screen {
    enum GameState {
        Starting,
        Running,
        Paused,
        GameOver
    }


    GameState state = GameState.Starting;
    World world;
    //Variables to know the the touch slice
    int xi = 0;
    int xf = 0;
    int yi = 0;
    int yf = 0;
    int distx = 0;
    int disty = 0;
    float ready = 0;

    public GameScreen(Game game) {
        super(game);
        world = new World();
    }

    @Override
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        if(state == GameState.Starting)
            updateStarting(touchEvents,deltaTime);
        if(state == GameState.Running)
            updateRunning(touchEvents, deltaTime);
        if(state == GameState.Paused)
            updatePaused(touchEvents);
        if(state == GameState.GameOver)
            updateGameOver(touchEvents);
    }
    private void updateStarting(List<TouchEvent> touchEvents, float deltaTime) {
        //if(touchEvents.size() > 0) state = GameState.Running;
        ready += deltaTime;
        //System.out.println(count);
        if(ready > 4){
            state = GameState.Running;
        }
    }

    private void getTouchInitial(TouchEvent event){
        xi = event.x;
        yi = event.y;
    }

    private void getTouchFinal(TouchEvent event){
        xf = event.x;
        yf = event.y;
        distx = xf-xi;
        disty = yf-yi;
    }
    private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_DOWN) {
                getTouchInitial(event);
            }

            if(event.type == TouchEvent.TOUCH_UP) {
                getTouchFinal(event);

                //Slide
                if(distx < -30)
                {
                    world.king.goLeft();
                }
                else if(distx > 30)
                {
                    world.king.goRight();
                }
                else if(disty < -30)
                {
                    world.king.goUp();
                }
                else if(disty > 30)
                {
                    world.king.goDown();
                }
                else if(event.x < 64 && event.y < 64) {
                    state = GameState.Paused;
                    return;
                }

            }
        }
        world.update(deltaTime);

        if(world.gameOver) {
            //Assets.xoc.play(1);
            state = GameState.GameOver;
        }

    }

    private void updatePaused(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x > 80 && event.x <= 240) {
                    if(event.y > 100 && event.y <= 148) {

                        state = GameState.Running;
                        return;
                    }
                    if(event.y > 148 && event.y < 196) {

                        game.setScreen(new MainMenuScreen(game));
                        return;
                    }
                }
            }
        }
    }

    private void updateGameOver(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x >= 128 && event.x <= 192 &&
                        event.y >= 200 && event.y <= 264) {
                    //Assets.click.play(1);
                    game.setScreen(new MainMenuScreen(game));
                    return;
                }
            }
        }
    }

    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.background, 0, 0);
        drawWorld(world);
        if(state == GameState.Starting)
            drawStartingUI();
        if(state == GameState.Running)
            drawRunningUI();
        if(state == GameState.Paused)
            drawPausedUI();
        if(state == GameState.GameOver)
            drawGameOverUI();

        drawText(g, Integer.toString(world.score), g.getWidth() / 2 - Integer.toString(world.score).length()*20 / 2, g.getHeight() - 42);

    }

    private void drawWorld(World world) {
        Graphics g = game.getGraphics();

        King king = world.king;

        Pixmap kingPixmap = null;
        if(world.lifes==3) {
            kingPixmap = Assets.king;
        }
        if(world.lifes==2){
            kingPixmap = Assets.king2;
        }
        if(world.lifes==1){
            kingPixmap = Assets.king3;
        }
        if(world.lifes==0){
            kingPixmap = Assets.king;
        }
        int x = king.x * 32;
        int y = king.y * 32;
        g.drawPixmap(kingPixmap, x , y );

        int len = world.noelList.size();
        for(int i = 0; i < len; i++) {
            Noel noelList = world.noelList.get(i);
            x = noelList.x * 32;
            y = noelList.y * 32;
            g.drawPixmap(Assets.noel, x, y);
        }

    }

    private void drawStartingUI() {
        Graphics g = game.getGraphics();
        //System.out.println("Starting");
        if(ready>0 && ready<1){
            g.drawPixmap(Assets.numbers, 5 *32-32 ,3*32,0,0,64,64);
        }
        if(ready>1 && ready<2){
            g.drawPixmap(Assets.numbers, 5*32-32 , 3*32,64,0,128,64);
        }
        if(ready>2 && ready<3){

            g.drawPixmap(Assets.numbers, 5*32-32 , 3*32,0,64,64,64);
        }
        if(ready>3){
            g.drawPixmap(Assets.numbers,5 *32-50 , 3*32,0,128,128,192);
        }



    }

    private void drawRunningUI() {
        Graphics g = game.getGraphics();
       // System.out.println("Running");
        g.drawPixmap(Assets.buttons, 0, 0, 64, 128, 64, 64);
    }

    private void drawPausedUI() {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.pause, 80, 100);

    }

    private void drawGameOverUI() {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.gameOver, 62, 100);
        g.drawPixmap(Assets.buttons, 128, 200, 0, 128, 64, 64);

    }

    public void drawText(Graphics g, String line, int x, int y) {
        int len = line.length();
        for (int i = 0; i < len; i++) {
            char character = line.charAt(i);

            if (character == ' ') {
                x += 20;
                continue;
            }

            int srcX = 0;
            int srcWidth = 0;
            if (character == '.') {
                srcX = 200;
                srcWidth = 10;
            } else {
                srcX = (character - '0') * 20;
                srcWidth = 20;
            }

            //g.drawPixmap(Assets.numbers, x, y, srcX, 0, srcWidth, 32);
            x += srcWidth;
        }
    }

    @Override
    public void pause() {
        if(state == GameState.Running)
            state = GameState.Paused;
    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}

